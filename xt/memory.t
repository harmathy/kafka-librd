#!/usr/bin/perl
use strict;
use warnings;
use Test::More;

use Test::Valgrind;

use Kafka::Librd;

my $logger = sub {
    my ($level, $facility, $message) = @_;
    printf(STDERR "[%s] (Lv %s): %s\n", $facility, $level, $message);
};

my $group_id = "test-consumer";
my $brokers = "localhost:9092";
my @topics = ("test");

foreach (1 .. 8) {
    my $kafka = Kafka::Librd->new(
        Kafka::Librd::RD_KAFKA_CONSUMER,
        {
            'log_cb'              => $logger,
            'group.id'            => $group_id,
            'api.version.request' => 'true',
            'debug'                 => 'all',
        },
    );

    my $added = $kafka->brokers_add($brokers);

    my $err = $kafka->subscribe(\@topics);
    if ($err != 0) {
        die "Couldn't subscribe: ", Kafka::Librd::Error::to_string($err);
    }

    my $msg = $kafka->consumer_poll(1000);

    $kafka->commit;

    $kafka->consumer_close;

    $kafka->destroy;

    Kafka::Librd::rd_kafka_wait_destroyed(5000);
}

done_testing();

